package tracker

import (
	"bytes"
	"crypto/sha512"
	"encoding/base64"
	"image"
	"image/color"
	"image/gif"
	"image/png"
	"io/ioutil"
	"math"
	"net/http"

	"github.com/fogleman/gg"
	"github.com/pkg/errors"
)

const emoteurl = "https://cdn.discordapp.com/emojis/"

const crunchWidth = 32
const crunchHeight = 32

var getcli = http.Client{}

// getEmoteData fetches an emote from the discord CDN
func getEmoteData(emojiID string, animated bool) ([]byte, error) {
	exten := ".png?v=1"
	if animated {
		exten = ".gif?v=1"
	}

	res, err := getcli.Get(emoteurl + emojiID + exten)
	if err != nil {
		return nil, errors.Wrap(err, "httpGet Emote")
	}

	dat, err := ioutil.ReadAll(res.Body)

	return dat, err
}

// genHash generates a base64 hash of some data using the sha512 algorithm
func genHash(dat []byte) string {
	sum64 := sha512.Sum512(dat)
	var sum = make([]byte, 64)
	for x := range sum64 {
		sum[x] = sum64[x]
	}

	hash := base64.RawURLEncoding.EncodeToString(sum)

	return hash
}

// qualityCruncher thinks your quality is delicious,
// so it eats the quality of some image bytes in either png or gif format and returns a version of it reduced to a 32x32 resolution and a reduced color palatte
// uses nnDownscale for downscaling and a 30 color palatte defined in cp
func qualityCruncher(data []byte, animated bool, width, height int) ([]byte, error) {
	if animated {
		g, err := gif.DecodeAll(bytes.NewReader(data))
		if err != nil {
			return nil, errors.Wrap(err, "gif.DecodeAll")
		}

		// Downscale the detail of each frame to the given resolution and reduce color detail to the color palatte defined in cp
		for x := range g.Image {
			d := nnDownscaler(g.Image[x], 32, 32)
			g.Image[x] = toPalatted(d, cp)
		}

		// set the settings to the new res
		g.Config.Height = 32
		g.Config.Width = 32

		// Reencode the gif file
		wr := bytes.NewBuffer([]byte{})
		err = gif.EncodeAll(wr, g)
		if err != nil {
			return nil, errors.Wrap(err, "gif.EncodeAll")
		}

		data = wr.Bytes()

	} else {
		p, err := png.Decode(bytes.NewReader(data))
		if err != nil {
			return nil, errors.Wrap(err, "png.Decode")
		}

		// Downscale the detail to the given resolution and reduce color detail to the color palatte defined in cp
		d := nnDownscaler(p, 32, 32)
		p = toPalatted(d, cp)

		// Reencode the png file
		wr := bytes.NewBuffer([]byte{})
		err = png.Encode(wr, p)
		if err != nil {
			return nil, errors.Wrap(err, "png.Encode")
		}

		data = wr.Bytes()

	}
	return data, nil
}

// nnDownscaler downscales an image to a specified size using an implementation of a nearest neighbor algorithm
func nnDownscaler(i image.Image, width, height int) image.Image {
	dc := gg.NewContext(width, height)

	xratio := float64(width) / float64(i.Bounds().Dx())
	yratio := float64(height) / float64(i.Bounds().Dy())

	for x := 0; x <= width; x++ {
		for y := 0; y <= height; y++ {

			dc.Push()
			dc.SetColor(i.At(intDiFloat(x, xratio), intDiFloat(y, yratio)))
			dc.SetPixel(x, y)
			dc.Pop()
		}
	}

	return dc.Image()
}

// cp is a 30 colour palatte (including transparent) that includes white, black, 3 greys, and 2 lightness levels and 2 saturation levels of red yellow green bluegreen blue and purple
var cp = color.Palette{
	color.Transparent, color.Black, color.White,

	// 3 grey colors
	color.RGBA{191, 191, 191, 255},
	color.RGBA{127, 127, 127, 255},
	color.RGBA{63, 63, 63, 255},

	// Max saturation Max value for the 6 colors
	color.RGBA{255, 0, 0, 255},
	color.RGBA{255, 255, 0, 255},
	color.RGBA{0, 255, 0, 255},
	color.RGBA{0, 255, 255, 255},
	color.RGBA{0, 0, 255, 255},
	color.RGBA{255, 0, 255, 255},

	// Max saturation Mid value for the 6 colors
	color.RGBA{127, 0, 0, 255},
	color.RGBA{127, 127, 0, 255},
	color.RGBA{0, 127, 0, 255},
	color.RGBA{0, 127, 127, 255},
	color.RGBA{0, 0, 127, 255},
	color.RGBA{127, 0, 127, 255},

	// Mid saturation Max value for the 6 colors
	color.RGBA{255, 127, 127, 255},
	color.RGBA{255, 255, 127, 255},
	color.RGBA{127, 255, 127, 255},
	color.RGBA{127, 255, 255, 255},
	color.RGBA{127, 127, 255, 255},
	color.RGBA{255, 127, 255, 255},

	// Mid saturation Mid value for the 6 colors
	color.RGBA{127, 63, 63, 255},
	color.RGBA{127, 127, 63, 255},
	color.RGBA{63, 127, 63, 255},
	color.RGBA{63, 127, 127, 255},
	color.RGBA{63, 63, 127, 255},
	color.RGBA{127, 63, 127, 255},
}

// toPalatted converts an image to a color palatted version of itself using a given color palatte
func toPalatted(i image.Image, c color.Palette) *image.Paletted {
	pl := image.NewPaletted(i.Bounds(), c)

	for x := 0; x <= i.Bounds().Dx(); x++ {
		for y := 0; y <= i.Bounds().Dy(); y++ {

			pl.Set(x, y, c.Convert(i.At(x, y)))
		}
	}
	return pl
}

// intDiFloat divides an int by a float value, and returns a rounded version of that as an int, effectively is shorthand for `int(math.Round(float64(a) / b))`
func intDiFloat(a int, b float64) int {
	return int(math.Round(float64(a) / b))
}
