package tracker

import (
	"fmt"
	"io/ioutil"
	"path"
	"regexp"
	"strings"
	"sync"
	"time"

	"codeberg.org/eviedelta/emotetracker/config"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

// "(?:(?:<:)|(?:<a:))((?:[a-z]|[A-Z]|_|[0-9]){2,})(?::)([0-9]+)(?:>)" old one that doesn't include the animated point

// emoteRegex defines a regex designed to identify the emote codepoints used by discord (eg <:name:1234578974232> or <a:name:13653436783678> )
// and extract the name, id, and animated status from that
var emoteRegex = regexp.MustCompile("(?:(?:<(a)?:))((?:[a-z]|[A-Z]|_|[0-9]){2,})(?::)([0-9]+)(?:>)")

// identifyEmotes takes a message string and identifies all of the discord emotes within that message, returning an array of emote objects describing such,
// takes a setting on whether or not to convert all names to lowercase, and a setting on whether or not to skip duplicates
func identifyEmotes(message string, tolower bool, skipdups bool) (emotes []Emote) {
	// identify all emotes within the string using the emoteRegex
	ls := emoteRegex.FindAllStringSubmatch(message, -1)
	if ls == nil {
		return
	}

	// initialise the slice of emote objects
	emotes = make([]Emote, 0, len(ls))

	// map used for marking whether or not emotes have already been found
	lst := make(map[string]bool, len(ls))

	for _, x := range ls {
		// if skip duplicats is enabled and this emote is already present than skip it
		if skipdups && lst[x[3]] {
			continue
		}
		lst[x[3]] = true

		emotes = append(emotes, Emote{
			ID:   x[3],
			Name: x[2],
		})

		el := len(emotes) - 1
		// if toLower is true modify the emote name to be lowercase
		if tolower {
			emotes[el].Name = strings.ToLower(emotes[el].Name)
		}
		// if the emote is animated mark is as so
		if x[1] == "a" {
			emotes[el].Animated = true
		}
	}

	return
}

var emoteSync sync.Mutex

// parseMessage takes a discord message object, and identifies all emotes within that message, returning a slice of message_emote objects,
// it takes a path to save the raw emote files to for debugging (possibly also caching for built in statistic commands if added?) and a time to mark the message as from
func parseMessage(m *discordgo.Message, saveto string, ts time.Time) (messages []Message, err error) {
	emotes := identifyEmotes(m.Content, true, true)

	emoteSync.Lock()
	defer emoteSync.Unlock()
	emotes, err = getEmotes(emotes, saveto)
	if err != nil {
		return nil, errors.Wrap(err, "identifyEmotes")
	}

	messages = make([]Message, 0, len(emotes))

	for _, x := range emotes {
		messages = append(messages, Message{
			Message:   m.ID,
			Guild:     m.GuildID,
			Channel:   m.ChannelID,
			User:      m.Author.ID,
			EmoteHash: x.Hash,
			EmoteID:   x.ID,
			Timestamp: ts,
		})
	}

	return messages, err
}

// getEmotes calls getEmote on every emote in a slice of emote objects
func getEmotes(emotes []Emote, saveto string) ([]Emote, error) {
	for i := range emotes {
		e, err := getEmote(emotes[i], saveto)
		if err != nil {
			return nil, errors.Wrap(err, "getEmote")
		}
		emotes[i] = e
	}

	return emotes, nil
}

// getEmote takes an emote object, and checks for it in the database, or downloads it and adds it to the database in order to fill out the hash fields of the object
func getEmote(emote Emote, saveto string) (Emote, error) {
	// attempt to fetch the emote from the database
	e, ok, err := getEmoteByID(emote.ID)
	if err != nil {
		return emote, errors.Wrap(err, "getEmoteByID")
	}
	// if its found set it to that, if its not then go through the steps of initialising the emote in the database
	if ok {
		emote = e
	} else {
		data, err := getEmoteData(emote.ID, emote.Animated)
		if err != nil {
			return emote, errors.Wrap(err, "getEmoteData")
		}

		emote.FullHash = genHash(data)

		datb, err := qualityCruncher(data, emote.Animated, crunchWidth, crunchHeight)
		if err != nil {
			return emote, errors.Wrap(err, "qualityCruncher")
		}

		emote.Hash = genHash(datb)

		// if saveto is enabled, check if an emote with this hash is already saved before saving it
		if saveto != "" && !config.CheckExist(path.Join(saveto, emote.FullHash)) {
			var ex = ".png"
			if emote.Animated {
				ex = ".gif"
			}
			// just print out the error if it fails to save, at this point this is more or less debug anyways and is not required to the function of the bot
			err := ioutil.WriteFile(path.Join(saveto, emote.FullHash+ex), data, 0644)
			if err != nil {
				fmt.Printf("error saving %v\n%v", path.Join(saveto, emote.FullHash), err)
			}
		}

		if err := addEmote(emote); err != nil {
			if Config.Debug {
				return emote, fmt.Errorf("addEmote(%v): %w", emote, err)
			}
			return emote, errors.Wrap(err, "addEmote")
		}
	}
	return emote, nil
}
