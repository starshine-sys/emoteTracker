package tracker

import (
	"codeberg.org/eviedelta/emotetracker/wlog"
	"github.com/bwmarrin/discordgo"
)

// messageUpdate updates the message emote data in the database to reflect the edited form of the message
func messageUpdate(s *discordgo.Session, m *discordgo.MessageUpdate) {
	_, err := handleMessage(m.Message, true)
	if err != nil {
		wlog.Err.Print(err)
		return
	}
}

// messageDelete deletes all messageemote data for a specific message from the database when a message is deleted
func messageDelete(s *discordgo.Session, d *discordgo.MessageDelete) {
	err := deleteMessagesByID(d.ID)
	if err != nil {
		wlog.Err.Print(err)
	}
}

// messageDeleteBul is like messageDelete but for bulk delete events
func messageDeleteBulk(s *discordgo.Session, d *discordgo.MessageDeleteBulk) {
	for _, m := range d.Messages {
		err := deleteMessagesByID(m)
		if err != nil {
			wlog.Err.Print(err)
		}
	}
}

// onMessage calls the message handler on a new message event
func onMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	_, err := handleMessage(m.Message, false)
	if err != nil {
		wlog.Err.Print(err)
		return
	}
}

// handleMessage goes through and runs the various functions needed for adding all the emotes in a message to the database
func handleMessage(m *discordgo.Message, overwrite bool) (int, error) {
	// calculate send time
	t, err := discordgo.SnowflakeTimestamp(m.ID)
	if err != nil {
		return 0, err
	}

	// pase message
	messages, err := parseMessage(m, Config.SaveTo, t.UTC())
	if err != nil {
		return 0, err
	}

	// if overwrite is enabled, check if there are emotes already in the database for this message, and if so delete them first
	if overwrite {
		_, ok, err := getMessagesByID(m.ID)
		if err != nil {
			return 0, err
		}
		if ok {
			deleteMessagesByID(m.ID)
		}
	}

	// if there are no emotes in the message return back
	if len(messages) < 1 {
		return 0, nil
	}

	return len(messages), addMessages(messages)
}
