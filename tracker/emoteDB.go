package tracker

import (
	"codeberg.org/eviedelta/emotetracker/wlog"
	"github.com/gocraft/dbr/v2"
)

// Emote defines internal data used for identifying emotes
type Emote struct {
	ID       string `db:"id"`
	Name     string `db:"name"`
	Animated bool   `db:"animated"`
	Hash     string `db:"hash"`
	FullHash string `db:"fullhash"`
}

// addEmote inserts an emote into the database
func addEmote(emote Emote) error {
	if Config.Debug {
		wlog.Info.Printf("inserting emote %v (%v) into the database", emote.Name, emote.ID)
	}
	_, err := ss.InsertInto("emotes").
		Columns("id", "name", "animated", "hash", "fullhash").
		Record(emote).Exec()
	return err
}

// getEmoteUniqueCount counts the amount of 'unique' emotes within the database
func getEmoteUniqueCount() (ct int, err error) {
	_, err = ss.Select("COUNT(DISTINCT hash)").
		From("emotes").
		Load(&ct)
	return
}

// getEmoteCount gets the amount of known emotes in total
func getEmoteCount() (ct int, err error) {
	_, err = ss.Select("COUNT(DISTINCT id)").
		From("emotes").
		Load(&ct)
	return
}

// getEmoteByID gets an emote from the database by its ID
func getEmoteByID(id string) (emote Emote, found bool, err error) {
	i, err := ss.Select("*").
		From("emotes").
		Where(dbr.Eq("id", id)).
		Load(&emote)

	if i < 1 {
		return
	}
	return emote, true, err
}

// getEmoteByShortHash gets all the emotes in the database with a specific short hash (short hash being the hash generated from a reduced detail form used for identifying)
func getEmoteByShortHash(hash string) (emote []Emote, found bool, err error) {
	i, err := ss.Select("*").
		From("emotes").
		Where(dbr.Eq("hash", hash)).
		Load(&emote)

	if i < 1 {
		return
	}
	return emote, true, err
}

// getEmoteByFullHash gets all the emotes in the database with a specific fullhash (fullhash being the hash generated from the full detail form of the emote)
func getEmoteByFullHash(hash string) (emote []Emote, found bool, err error) {
	i, err := ss.Select("*").
		From("emotes").
		Where(dbr.Eq("fullhash", hash)).
		Load(&emote)

	if i < 1 {
		return
	}
	return emote, true, err
}
