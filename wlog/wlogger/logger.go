package wlogger

import (
	"fmt"
	"log"

	"codeberg.org/eviedelta/emotetracker/wlog/dwh"
)

// Logger is a logger
type Logger struct {
	Color  int
	Avatar string
	Name   string
	Status string

	Webhook *dwh.Webhook
}

// Send sends a single string to the webhook
func (l *Logger) Send(s string) error {
	log.Println(s)
	if l.Webhook.URL == "" {
		return nil
	}

	err := l.Webhook.SendMessage(dwh.WebhookMessage{
		Username:  l.Name,
		AvatarURL: l.Avatar,

		Embeds: []dwh.Embed{{
			Title:       l.Status,
			Description: s,
			Color:       l.Color,
		}},
	})
	if err != nil {
		log.Println(err)
	}
	return err
}

// Print prints a message to the webhook using the logic from fmt.Sprint()
func (l *Logger) Print(in ...interface{}) error {
	return l.Send(fmt.Sprint(in...))
}

// Printf prints a message to the webhook using the logic from fmt.Sprintf()
func (l *Logger) Printf(s string, in ...interface{}) error {
	return l.Send(fmt.Sprintf(s, in...))
}
