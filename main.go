package main

import (
	"flag"
	"log"
	"os"

	"codeberg.org/eviedelta/emotetracker/config"
	"codeberg.org/eviedelta/emotetracker/gcmd"
	"codeberg.org/eviedelta/emotetracker/module"
	"codeberg.org/eviedelta/emotetracker/tracker"
	"codeberg.org/eviedelta/emotetracker/wlog"
)

const botSoftware = "emoteTracker"
const botVersion = "0.1.3.1"

// main, starts stuff
func main() {
	modules := []*module.Module{
		gcmd.Module,
		tracker.Module,
	}

	setup(modules)
	startUntilStop(modules)
}

// some initialisation stuff, these are done within init because they are expected to be always ready
func init() {
	// set the config file location (it has some defaults to check so you don't need to specify with the flag if its in ./config.toml or ./data/config.toml)
	flag.StringVar(&flagConfig, "config", "", "the config file to use for the main config")
	flag.StringVar(&flagDataDir, "data", "", "the location to look for data in")
	flag.Parse()

	if flagDataDir != "" {
		err := os.Chdir(flagDataDir)
		if err != nil {
			panic(err)
		}
	}

	// load le config
	cfg, err := config.AConf(flagConfig)
	if err != nil {
		log.Fatal("Configuration error", err)
	}
	conf = cfg

	wlog.Err.Webhook.URL = conf.Logging.Webhooks.Errs
	wlog.Err.Name = conf.Logging.NamePrefix + ": Errors"
	wlog.Err.Avatar = conf.Logging.ErrsPfp
	wlog.Info.Webhook.URL = conf.Logging.Webhooks.Info
	wlog.Info.Name = conf.Logging.NamePrefix + ": Info"
	wlog.Info.Avatar = conf.Logging.InfoPfp

	if conf.Bot.AdminCommandName == "" {
		conf.Bot.AdminCommandName = "admin"
	}
	if conf.Bot.DebugCommandName == "" {
		conf.Bot.DebugCommandName = "debug"
	}
}
