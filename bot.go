package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/drc/detc"
	"codeberg.org/eviedelta/drc/subpresets"
	"codeberg.org/eviedelta/emotetracker/config"
	"codeberg.org/eviedelta/emotetracker/module"
	"codeberg.org/eviedelta/emotetracker/wlog"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
)

var conf config.Config

// Dg is the main discord session
var Dg *discordgo.Session

// Hn is the global handler
var Hn *drc.Handler

var adminCommands *drc.Command
var debugCommands *drc.Command

// flags variables for some global configuration
var (
	flagConfig  string
	flagDataDir string
)

func onReady(s *discordgo.Session, r *discordgo.Ready) {
	err := wlog.Info.Printf("onReady Event Received, Bot running and connected to Discord.\nAccount: %v (%v)", Dg.State.User.String(), Dg.State.User.ID)
	if err != nil {
		log.Println(err)
		err = wlog.Err.Printf("Error sending log message: %v", err)
		if err != nil {
			log.Println(Dg.UpdateListeningStatus("LOGGING ERROR, PLEASE REPORT TO ADMIN"))
		}
		log.Println(err)
	}
}

// this is lazy, i'll rewrite this to be prettier eventually
func setup(modules []*module.Module) {
	var err error

	// make a new bot session
	Dg, err = discordgo.New("Bot " + conf.Auth.Token)
	if err != nil {
		log.Fatalln("Error creating discord session", err)
	}

	Dg.Identify.Intents = nil

	// initialise handler session
	Hn = drc.NewHandler(drc.CfgHandler{
		Prefixes: conf.Bot.Prefix,
		Admins:   conf.Bot.Admins,

		ReplyOn: drc.ActOn{
			Error:   trit.True,
			Denied:  trit.True,
			Failure: trit.True,
		},
		ReactOn: drc.ActOn{
			Error:   trit.True,
			Denied:  trit.True,
			Failure: trit.True,
		},

		DefaultSubcommands: []drc.Command{
			subpresets.SubcommandSimpleHelp,
		},
	}, Dg)

	// Initialise debug and admin commands
	adminCommands = Hn.Commands.Add(&drc.Command{
		Name: "ADMIN",
		Permissions: drc.Permissions{
			BotAdmin: trit.True,
		},
		Exec: func(ctx *drc.Context) error {
			// this is crappy but its admin only anyways
			return ctx.Reply("```\n", detc.FormatterThing(fmt.Sprint(ctx.Com.Subcommands.ListRecursive(true, 2, false))), "\n```")
		},
	})
	debugCommands = Hn.Commands.Add(&drc.Command{
		Name: "DEBUG",
		Permissions: drc.Permissions{
			BotAdmin: trit.True,
		},
		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("```\n", detc.FormatterThing(fmt.Sprint(ctx.Com.Subcommands.ListRecursive(true, 2, false))), "\n```")
		},
	})
	Hn.Commands.AddAliasString(conf.Bot.AdminCommandName, "ADMIN")
	Hn.Commands.AddAliasString(conf.Bot.DebugCommandName, "DEBUG")

	// add the core handlers (guild create mostly just exists for debug at this point)
	Dg.AddHandler(onMessage)
	Dg.AddHandler(guildCreate)
	Dg.AddHandler(onReady)

	var modlist string

	// Load up all the provided modules
	for _, m := range modules {
		modlist += m.Name + "\n"
		if m.Config != nil {
			if err := config.AnyConf(flagDataDir, m.Name+".toml", m.Config); err != nil && os.IsNotExist(err) {
				panic(err)
			} else if err == nil {
				m.ConfigFound = true
			}
		}

		if m.InitFunc != nil {
			err := m.InitFunc(m)
			if err != nil {
				panic(err)
			}
		}

		// Add all commands
		Hn.Commands.AddBulk(m.Commands)

		// Add admin commands
		adminCommands.Subcommands.AddBulk(m.AdminCommands)

		// Add debug commands only if debug commands are enabled
		if conf.Bot.EnableDebugCommands {
			debugCommands.Subcommands.AddBulk(m.DebugCommands)
		}

		// if no extra discordgo handlers were provided skip and contine
		if len(m.DgoHandlers) < 1 {
			continue
		}
		// else add those to the discordgo session
		for _, h := range m.DgoHandlers {
			Dg.AddHandler(h)
		}
	}

	// Initialise the handler
	err = Hn.Ready()
	if err != nil {
		log.Fatalln("Error initialising handler", err)
	}

	err = wlog.Info.Printf("Setup bot with modules:```\v%v```\nRunning DRC: %v\nBot Software: %v v%v", modlist, drc.Version, botSoftware, botVersion)

	if err != nil {
		err2 := wlog.Err.Print("Error sending init log", err)
		if err != nil {
			log.Println(err2)
		}
		log.Fatalln(err)
	}
}

// starts the bot and runs it until something requests it to close
func startUntilStop(modules []*module.Module) {
	for _, m := range modules {
		if m.OpenFunc != nil {
			err := m.OpenFunc(m)
			if err != nil {
				panic(err)
			}
		}
		if m.CloseFunc != nil {
			defer m.CloseFunc(m)
		}
	}

	// Connect to discord
	err := Dg.Open()
	if err != nil {
		log.Fatalln("Error opening connection", err)
	}
	// Disconnect once everything is done
	defer Dg.Close()

	defer func() {
		log.Println(wlog.Info.Printf("Bot Shutting Down..."))
	}()

	// Wait until a shutdown signal is recived
	fmt.Println("Bot Running, Press CTRL-C to Shutdown")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

// guildCreate just logs all guilds added for debug purposes
func guildCreate(s *discordgo.Session, g *discordgo.GuildCreate) {
	if conf.Bot.Debugm {
		log.Println("guild create: ", g.Name, " (", g.ID, ")")
	}
}
