-- Mojishia pSQL layout v0.2

-- the database code will need improvement by somebody who actually knows how to sql (copied from pgAdmins view sql thing)

CREATE TABLE public.emotes
(
    id text COLLATE pg_catalog."default" NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    hash text COLLATE pg_catalog."default" NOT NULL,
    fullhash text COLLATE pg_catalog."default" NOT NULL,
    animated boolean NOT NULL,
    CONSTRAINT emotes_pkey PRIMARY KEY (id)
);

CREATE TABLE public.messages
(
    message_id text COLLATE pg_catalog."default" NOT NULL,
    emote_id text COLLATE pg_catalog."default" NOT NULL,
    emote_hash text COLLATE pg_catalog."default" NOT NULL,
    guild_id text COLLATE pg_catalog."default" NOT NULL,
    channel_id text COLLATE pg_catalog."default" NOT NULL,
    user_id text COLLATE pg_catalog."default" NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    CONSTRAINT emote FOREIGN KEY (emote_id)
        REFERENCES public.emotes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.reactions
(
    message_id text COLLATE pg_catalog."default" NOT NULL,
    emote_id text COLLATE pg_catalog."default" NOT NULL,
    emote_hash text COLLATE pg_catalog."default" NOT NULL,
    guild_id text COLLATE pg_catalog."default" NOT NULL,
    channel_id text COLLATE pg_catalog."default" NOT NULL,
    user_id text COLLATE pg_catalog."default" NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    CONSTRAINT emote FOREIGN KEY (emote_id)
        REFERENCES public.emotes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)