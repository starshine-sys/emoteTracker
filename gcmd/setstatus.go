package gcmd

import (
	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
)

var setstatus = &drc.Command{
	Name:         "setstatus",
	Manual:       []string{"sets status"},
	CommandPerms: discordgo.PermissionSendMessages,
	Permissions: drc.Permissions{
		BotAdmin: trit.True,
	},
	Config: drc.CfgCommand{
		Listable: false,
		ReactOn: drc.ActOn{
			Success: trit.True,
		},
		MinimumArgs: 1,
	},
	Exec: fSetstatus,
}

func fSetstatus(ctx *drc.Context) error {
	var state = 1
	if len(ctx.RawArgs) >= 2 && ctx.RawArgs[1] == "to" {
		state = 2
		if len(ctx.RawArgs) < 3 {
			return drc.NewParseError(nil, "Out of args")
		}
	}

	var str string
	do := func() {
		for i := state; i < len(ctx.RawArgs); i++ {
			str = str + ctx.RawArgs[i] + " "
		}
	}

	switch ctx.RawArgs[0] {
	default:
		state = 0
		fallthrough
	case "playing":
		do()
		return ctx.Ses.UpdateStatus(0, str)
	case "listening":
		do()
		return ctx.Ses.UpdateListeningStatus(str)
	}
}
