package gcmd

import (
	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
)

// CList is a command that lists all commands
var CList = &drc.Command{
	Name:         "commands",
	Manual:       []string{"lists commands"},
	CommandPerms: discordgo.PermissionSendMessages,
	Permissions: drc.Permissions{
		BotAdmin: trit.True,
	},
	Config: drc.CfgCommand{
		Listable: false,
		ReactOn: drc.ActOn{
			Success: trit.True,
		},
		MinimumArgs: 0,
	},
	Exec: fList,
}

func fList(ctx *drc.Context) error {
	return ctx.Reply("```\n", ctx.Han.Commands.List(false), "\n```")
}
