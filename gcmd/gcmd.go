package gcmd

import (
	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/emotetracker/module"
)

// Module contains stuff (bug me to fix comments later)
var Module = &module.Module{
	Name: "gcmd",
	Commands: []*drc.Command{
		Echo,
		CList,
	},

	AdminCommands: []*drc.Command{
		setstatus,
	},
}
